using BattleShips.Services;

namespace BattleShips.Events
{
    public class AttackResultEvent : IEvent
    {
        public int AttackingPlayerId { get; private set; }
        public AttackResult Result { get; private set; }
        public int NextPlayerId { get; private set; }

        public AttackResultEvent(int attackingPlayerId, AttackResult attackResult, int nextPlayerId)
        {
            AttackingPlayerId = attackingPlayerId;
            Result = attackResult;
            NextPlayerId = nextPlayerId;
        }

        public string EventName()
        {
            return "GameStateUpdated";
        }
    }
}