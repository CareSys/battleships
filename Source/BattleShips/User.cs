﻿using BattleShips.Services;

namespace BattleShips
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
    }
}