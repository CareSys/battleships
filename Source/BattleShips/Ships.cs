﻿using System.Collections.Generic;

namespace BattleShips
{
    public class Ship
    {
        protected Ship(string name, int length)
        {
            Name = name;
            Length = length;
        }

        public string Name { get; private set; }
        public int Length { get; private set; }

        public static Ship AircraftCarrier
        {
            get { return new Ship("Aircraft Carrier", 5); }
        }

        public static Ship Battleship
        {
            get { return new Ship("Battleship", 4); }
        }

        public static Ship Submarine
        {
            get { return new Ship("Submarine", 3); }
        }

        public static Ship Cruiser
        {
            get { return new Ship("Cruiser", 3); }
        }

        public static Ship PatrolBoat
        {
            get { return new Ship("Patrol Boat", 2); }
        }

        public static IEnumerable<Ship> DefaultShips
        {
            get
            {
                return new[]
                    {
                        AircraftCarrier, Battleship, Submarine, Cruiser, PatrolBoat
                    };
            }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", Name, Length);
        }
    }
}