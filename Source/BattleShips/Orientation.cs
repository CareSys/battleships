﻿namespace BattleShips
{
    public enum Orientation
    {
        Horizontal,
        Vertical
    }

    public enum PlayerDesignation
    {
        Player1 = 1,
        Player2
    }
}