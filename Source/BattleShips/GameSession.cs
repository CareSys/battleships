﻿using System;
using BattleShips.Services;

namespace BattleShips
{
    public class GameSession : IEntity
    {
        public int Id { get; set; }
        public bool IsOpen { get; set; }
        public bool IsActive { get; set; }
        public User PrimaryUser { get; set; }
        public User SecondaryUser { get; set; }
        public DateTime DateCreated { get; set; }

        public GameSession()
        {
        }

        public GameSession(User primaryUser)
        {
            IsOpen = true;
            IsActive = true;
            PrimaryUser = primaryUser;
        }

        public void AddSecondUser(User user2)
        {
            SecondaryUser = user2;
            IsOpen = false;
        }

        public void EndSession()
        {
            IsActive = false;
        }
    }
}