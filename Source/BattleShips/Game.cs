﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleShips.Services;

namespace BattleShips
{
    public class Game : IEntity
    {
        public int Id { get; set; }

        public int PlayerId1 { get; set; }
        public int PlayerId2 { get; set; }

        public int Height { get; set; }
        public int Width { get; set; }

        public IEnumerable<Coordinate> Player1Ships { get; set; }
        public IEnumerable<Coordinate> Player2Ships { get; set; }
        public IList<Coordinate> Player1Shots { get; set; }
        public IList<Coordinate> Player2Shots { get; set; }
        public PlayerDesignation CurrentPlayer { get; set; }
        public GameState State { get; set; }

        public Game()
        {
            Width = 10;
            Height = 10;

            Player1Ships = new HashSet<Coordinate>();
            Player2Ships = new HashSet<Coordinate>();
            Player1Shots = new List<Coordinate>();
            Player2Shots = new List<Coordinate>();
        }

        public void AddShip(PlayerDesignation player, Ship ship, Coordinate coordinate, Orientation orientation)
        {
            var coordinates = orientation == Orientation.Horizontal
                                  ? BuildHorizontalCoordinates(coordinate, ship.Length).ToArray()
                                  : BuildVerticalCoordinates(coordinate, ship.Length).ToArray();

            if (coordinates.Any(c => c.X > Width || c.Y > Height || c.X < 1 || c.Y < 1))
            {
                throw new InvalidOperationException("Can place ship off the board");
            }

            if (player == PlayerDesignation.Player1)
            {
                if (Player1Ships.Intersect(coordinates).Any())
                {
                    throw new InvalidOperationException("Can't add overlapping ships");
                }

                Player1Ships = Player1Ships.Concat(coordinates);
            }
            else
            {
                if (Player2Ships.Intersect(coordinates).Any())
                {
                    throw new InvalidOperationException("Can't add overlapping ships");
                }

                Player2Ships = Player2Ships.Concat(coordinates);
            }
        }

        public AttackResult Attack(PlayerDesignation playerDesignation, Coordinate coordinate)
        {
            if (playerDesignation != CurrentPlayer)
            {
                throw new InvalidOperationException(string.Format("Not {0}'s turn", playerDesignation));
            }

            if (coordinate.X > Width || coordinate.Y > Height)
            {
                throw new InvalidOperationException(string.Format("Coordinates not allowed"));
            }

            IEnumerable<Coordinate> applicableShips;
            IList<Coordinate> applicableShots;
            PlayerDesignation nextPlayer;
            
            switch (playerDesignation)
            {
                case PlayerDesignation.Player1:
                    applicableShips = Player2Ships.ToArray();
                    applicableShots = Player2Shots;
                    nextPlayer = PlayerDesignation.Player2;
                    break;
                case PlayerDesignation.Player2:
                    applicableShips = Player1Ships.ToArray();
                    applicableShots = Player1Shots;
                    nextPlayer = PlayerDesignation.Player1;

                    break;
                default:
                    throw new ArgumentOutOfRangeException("playerDesignation");
            }

            if (applicableShots.Contains(coordinate))
            {
                throw new InvalidOperationException(string.Format("Player has already shot at {0}", coordinate));
            }

            applicableShots.Add(coordinate);

            AttackResult result;

            if (!applicableShips.Contains(coordinate))
            {
                result = AttackResult.Miss;
                CurrentPlayer = nextPlayer;
            }
            else if (applicableShips.All(c => applicableShots.Contains(c)))
            {
                result = AttackResult.HitAndWin;
                State = GameState.Finished;
            }
            else
            {
                result = AttackResult.Hit;
            }

            return result;
        }
        
        private IEnumerable<Coordinate> BuildHorizontalCoordinates(Coordinate startPoint, int length)
        {
            return Enumerable.Range(0, length).Select(x => new Coordinate(startPoint.X + x, startPoint.Y));
        }

        private IEnumerable<Coordinate> BuildVerticalCoordinates(Coordinate startPoint, int length)
        {
            return Enumerable.Range(0, length).Select(x => new Coordinate(startPoint.X, startPoint.Y +x));
        }
    }
}