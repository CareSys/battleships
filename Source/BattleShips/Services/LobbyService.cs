﻿using System.Collections.Generic;
using System.Linq;
using BattleShips.ViewModels.Lobby;

namespace BattleShips.Services
{
    public interface ILobbyService
    {
        IEnumerable<GameSessionInformation> GetCurrentGames(int userId);
    }

    public class LobbyService : ILobbyService
    {
        private readonly IRepository _repository;
        private readonly IPublishService _publishService;

        public LobbyService(IRepository repository, IPublishService publishService)
        {
            _repository = repository;
            _publishService = publishService;
        }

        public IEnumerable<GameSessionInformation> GetCurrentGames(int userId)
        {
            var sessions = _repository.Query<GameSession>()
                .Where(gs => (gs.PrimaryUser != null && gs.PrimaryUser.Id == userId) ||
                             (gs.SecondaryUser != null && gs.SecondaryUser.Id == userId))
                .Where(gs => gs.IsActive)
                .OrderByDescending(gs => gs.DateCreated)
                .Select(gs => new GameSessionInformation
                {
                    GameSessionId = gs.Id,
                    DateCreated = gs.DateCreated,
                    Player1Name = gs.PrimaryUser != null ? gs.PrimaryUser.UserName : null,
                    Player2Name = gs.SecondaryUser != null ? gs.SecondaryUser.UserName : null
                });

            return sessions;
        }
    }
}