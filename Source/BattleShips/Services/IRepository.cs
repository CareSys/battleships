﻿using System.Linq;

namespace BattleShips.Services
{
    public interface IRepository
    {
        T Get<T>(int id) where T : class, IEntity;
        IQueryable<T> Query<T>() where T : class, IEntity;
        void CommitChanges();
        void Insert<T>(T item) where T : class, IEntity;
    }

    public interface IEntity
    {
        int Id { get; }
    }
}