﻿using System;
using BattleShips.Events;

namespace BattleShips.Services
{
    public interface IGamePlayService
    {
        void Attack(int playerId, int gameId, Coordinate attackCoordinate);
    }

    public class GamePlayService : IGamePlayService
    {
        private readonly IRepository _repository;
        private readonly IPublishService _publishService;

        public GamePlayService(IRepository repository, IPublishService publishService)
        {
            _repository = repository;
            _publishService = publishService;
        }
        
        public void Attack(int playerId, int gameId, Coordinate attackCoordinate)
        {
            var game = _repository.Get<Game>(gameId);

            var playerDesignation = GetPlayerDesignation(game, playerId);

            var result = game.Attack(playerDesignation, attackCoordinate);

            var e = new AttackResultEvent(playerId, result, GetPlayerId(game, game.CurrentPlayer));

            _publishService.PublishToUser(game.PlayerId1, e);
            _publishService.PublishToUser(game.PlayerId2, e);
        }

        private static PlayerDesignation GetPlayerDesignation(Game game, int playerId)
        {
            if (game.PlayerId1 == playerId) return PlayerDesignation.Player1;
            if (game.PlayerId2 == playerId) return PlayerDesignation.Player2;

            throw new InvalidOperationException("Player is neither player 1 or player 2");
        }
        
        private static int GetPlayerId(Game game, PlayerDesignation playerDesignation)
        {
            switch (playerDesignation)
            {
                case PlayerDesignation.Player1:
                    return game.PlayerId1;
                case PlayerDesignation.Player2:
                    return game.PlayerId2;
                default:
                    throw new ArgumentOutOfRangeException("playerDesignation");
            }
        }
    }
}