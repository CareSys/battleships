﻿namespace BattleShips.Services
{
    public interface IGameSetupService
    {
        void SetupGame(int player1Id, int player2Id);
    }

    public class GameSetupService : IGameSetupService
    {
        private readonly IRepository _repository;
        private readonly IPublishService _publishService;

        public GameSetupService(IRepository repository, IPublishService publishService)
        {
            _repository = repository;
            _publishService = publishService;
        }

        //ask both players to put their ships on the board
        //tell them off if they put ships in silly places
        //tell both users when the game is ready to start

        public void SetupGame(int player1Id, int player2Id)
        {
            throw new System.NotImplementedException();
        }
    }
}