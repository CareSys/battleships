﻿namespace BattleShips.Services
{
    public interface IPublishService
    {
        void PublishToUser(int userId, IEvent data);
        void PublishToAll(IEvent data);
    }

    public interface IEvent
    {
        string EventName();
    }
}