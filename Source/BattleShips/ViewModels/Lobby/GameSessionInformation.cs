using System;

namespace BattleShips.ViewModels.Lobby
{
    public class GameSessionInformation
    {
        public int GameSessionId { get; set; }
        public string Player1Name { get; set; }
        public string Player2Name { get; set; }
        public DateTime DateCreated { get; set; }
    }
}