﻿namespace BattleShips
{
    public interface IPublisher
    {
        void PublishAttackResult(AttackResult result);         
    }
}