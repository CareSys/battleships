﻿namespace BattleShips
{
    public enum AttackResult
    {
        Miss,
        Hit,
        HitAndWin
    }
}