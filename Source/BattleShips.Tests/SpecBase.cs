﻿using NUnit.Framework;

namespace BattleShips.Tests
{
    public abstract class SpecBase
    {
        protected SpecBase()
        {
            Given();
            When();
        }

        protected virtual void When()
        {
            //Do nothing
        }

        protected virtual void Given()
        {
            //Do nothing
        }
    }

    public class ThenAttribute : TestAttribute
    {
        
    }
}