﻿using System.Collections.Generic;
using System.Linq;
using BattleShips.Services;

namespace BattleShips.Tests
{
    public class FakeRepository : IRepository
    {
        public List<IEntity> DataStore { get; set; }
        public bool ChangesCommitted { get; set; }

        public FakeRepository()
        {
            DataStore = new List<IEntity>();
        }
        
        public T Get<T>(int id) where T : class, IEntity
        {
            return DataStore.OfType<T>().FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<T> Query<T>() where T : class, IEntity
        {
            return DataStore.OfType<T>().AsQueryable();
        }
        
        public void CommitChanges()
        {
            ChangesCommitted = true;
        }

        public void Insert<T>(T item) where T : class, IEntity
        {
            DataStore.Add(item);
        }
    }
}