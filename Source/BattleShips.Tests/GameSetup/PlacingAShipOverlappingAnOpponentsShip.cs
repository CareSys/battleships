﻿using System;
using NUnit.Framework;

namespace BattleShips.Tests.GameSetup
{
    public class PlacingAShipOverlappingAnOpponentsShip : ShipPlacementSpecBase
    {
        protected override void Given()
        {
            GivenABoard(10, 10);
            GivenAShipAt(PlayerDesignation.Player1, Ship.AircraftCarrier, new Coordinate(3, 3), Orientation.Horizontal);
        }

        protected override void When()
        {
            try
            {
                WhenPlacing(PlayerDesignation.Player2, Ship.Battleship, new Coordinate(3, 3), Orientation.Horizontal);
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }

        [Then]
        public void ThereShouldBeNoError()
        {
            Assert.IsNull(Exception);
        }
    }
}