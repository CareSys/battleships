﻿using System;
using NUnit.Framework;

namespace BattleShips.Tests.GameSetup
{
    public class PlacingAShipOffTheBoardOnOppositeSide : ShipPlacementSpecBase 
    {
        protected override void Given()
        {
            GivenABoard(10, 10);
        }

        protected override void When()
        {
            try
            {
                WhenPlacing(PlayerDesignation.Player1, Ship.AircraftCarrier, new Coordinate(0, 0), Orientation.Vertical);
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }

        [Then]
        public void ThereShouldBeAnError()
        {
            Assert.IsNotNull(Exception);
        } 
    }
}