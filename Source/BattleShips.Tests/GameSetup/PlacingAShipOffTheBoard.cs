﻿using System;
using NUnit.Framework;

namespace BattleShips.Tests.GameSetup
{
    public class PlacingAShipOffTheBoard : ShipPlacementSpecBase
    {
        protected override void Given()
        {
            GivenABoard(10, 10);
        }

        protected override void When()
        {
            try
            {
                WhenPlacing(PlayerDesignation.Player1, Ship.AircraftCarrier, new Coordinate(1, 10), Orientation.Vertical);
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }

        [Then]
        public void ThereShouldBeNoError()
        {
            Assert.IsNotNull(Exception);
        }
    
    }

}