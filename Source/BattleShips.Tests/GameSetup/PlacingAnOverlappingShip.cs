﻿using System;
using NUnit.Framework;

namespace BattleShips.Tests.GameSetup
{
    public class PlacingAnOverlappingShip : ShipPlacementSpecBase
    {
        protected override void Given()
        {
            GivenABoard(10, 10);
            GivenAShipAt(PlayerDesignation.Player2, Ship.AircraftCarrier, new Coordinate(3, 3), Orientation.Horizontal);
        }

        protected override void When()
        {
            try
            {
                WhenPlacing(PlayerDesignation.Player2,Ship.Battleship, new Coordinate(4, 2), Orientation.Vertical);
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }

        [Then]
        public void ThereShouldBeAnError()
        {
            Assert.IsNotNull(Exception);
        }
    }
}