﻿using System;

namespace BattleShips.Tests.GameSetup
{
    public abstract class ShipPlacementSpecBase : SpecBase
    {
        protected Game Game;
        protected Exception Exception;
        protected PlayerDesignation Player;

        protected void GivenABoard(int width, int height)
        {
            Game = new Game() {Height = height, Width = width};
        }

        protected void GivenAShipAt(PlayerDesignation player, Ship ship, Coordinate coordinate, Orientation orientation)
        {
            Game.AddShip(player, ship, coordinate, orientation);
        }

        protected void WhenPlacing(PlayerDesignation player, Ship ship, Coordinate coordinate, Orientation orientation)
        {
            Player = player;
            Game.AddShip(player, ship, coordinate, orientation);
        }
    }
}