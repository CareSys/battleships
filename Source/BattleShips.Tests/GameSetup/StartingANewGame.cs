﻿using BattleShips.Services;
using Moq;

namespace BattleShips.Tests.GameSetup
{
    public class StartingANewGame : SpecBase
    {
        protected int Player1Id;
        protected int Player2Id;

        protected GameSetupService GameSetupService;
        protected Mock<IRepository> Repository = new Mock<IRepository>();
        protected Mock<IPublishService> PublishService = new Mock<IPublishService>();

        protected override void Given()
        {
            Player1Id = 1;
            Player2Id = 2;
        }

        protected override void When()
        {
            GameSetupService = new GameSetupService(Repository.Object, PublishService.Object);

            GameSetupService.SetupGame(Player1Id, Player2Id);
        }

        [Then]
        public void ANewGameShouldBeSavedToDatabase()
        {
            Repository.Verify(r => r.Insert(It.IsAny<Game>()));
        }

        [Then]
        public void BothPlayersShouldBeAskedToAddShips()
        {
            
        }
    }
}