﻿
using System;
using System.Linq;
using NUnit.Framework;

namespace BattleShips.Tests.GameSetup
{
    public class PlacingFirstShip : ShipPlacementSpecBase
    {
        protected override void Given()
        {
            GivenABoard(10, 10);
        }

        protected override void When()
        {
            try
            {
                WhenPlacing(PlayerDesignation.Player1, Ship.AircraftCarrier, new Coordinate(1, 1), Orientation.Horizontal);
            }
            catch (Exception e)
            {
                Exception = e;
            }
        }

        [Then]
        public void ThereShouldBeNoError()
        {
            Assert.IsNull(Exception);
        }

        [Then]
        public void ThenTheShipCoordinatesShouldBePopulated()
        {
            if (Player == PlayerDesignation.Player1)
            {
                Assert.AreEqual(5, Game.Player1Ships.Count());

                Assert.IsTrue(Game.Player1Ships.Contains(new Coordinate(1, 1)));
                Assert.IsTrue(Game.Player1Ships.Contains(new Coordinate(2, 1)));
                Assert.IsTrue(Game.Player1Ships.Contains(new Coordinate(3, 1)));
                Assert.IsTrue(Game.Player1Ships.Contains(new Coordinate(4, 1)));
                Assert.IsTrue(Game.Player1Ships.Contains(new Coordinate(5, 1)));
            }
        }
    }
}