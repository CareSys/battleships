﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleShips.Services;
using BattleShips.ViewModels.Lobby;
using NUnit.Framework;

namespace BattleShips.Tests.Lobby
{
    public class ListingCurrentGames : LobbySpecBase
    {
        private IEnumerable<GameSessionInformation> _result;
        private GameSession _expectedResult1;
        private GameSession _expectedResult2;

        protected override void Given()
        {
            InitialiseService();

            var otherUser = GivenUserExists("OtherUser");

            GivenGameExists(otherUser, new DateTime(2010, 1, 1));
            GivenClosedGameExists(otherUser);

            _expectedResult1 = GivenGameExists(CurrentUser, new DateTime(2014, 4, 4));
            _expectedResult2 = GivenGameExists(CurrentUser, new DateTime(2014, 4, 5), otherUser);
            GivenClosedGameExists(CurrentUser);
            GivenClosedGameExists(CurrentUser, otherUser);
        }

        protected override void When()
        {
            _result = LobbyService.GetCurrentGames(CurrentUser.Id);
        }

        [Then]
        public void OnlyActiveGamesForCurrentUserAreReturned()
        {
            Assert.IsNotNull(_result);
            Assert.AreEqual(2, _result.Count());
        }

        [Then]
        public void ResultsAreOrderedByDateCreatedDescending()
        {
            CollectionAssert.IsOrdered(
                _result
                    .Reverse()
                    .Select(gi => gi.DateCreated));
        }

        [Then]
        public void ReturnedGamesContainsCorrectDetails()
        {
            var result1 = _result.First();
            var result2 = _result.Skip(1).First();
            
            Assert.AreEqual(_expectedResult1.Id, result1.GameSessionId);
            Assert.AreEqual(CurrentUser.UserName, result1.Player1Name);
            Assert.AreEqual("OtherUser", result1.Player2Name);

            Assert.AreEqual(_expectedResult2.Id, result2.GameSessionId);
            Assert.AreEqual(CurrentUser.UserName, result2.Player1Name);
            Assert.AreEqual(null, result2.Player2Name);
        }
    }
}