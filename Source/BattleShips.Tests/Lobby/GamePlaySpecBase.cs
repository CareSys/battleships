﻿using System;
using BattleShips.Services;
using Moq;

namespace BattleShips.Tests.Lobby
{
    public class LobbySpecBase : SpecBase
    {
        protected Mock<IPublishService> PublishService = new Mock<IPublishService>();
        protected readonly FakeRepository Repository = new FakeRepository();
        protected LobbyService LobbyService;
        
        protected void InitialiseService()
        {
            LobbyService = new LobbyService(Repository, PublishService.Object);
        }

        protected User CurrentUser { get { return new User {Id = 99, UserName = "CurrentUser"}; }}

        protected User GivenUserExists(string username)
        {
            var user = new User
            {
                Id = -1,
                UserName = username
            };

            Repository.Insert(user);

            return user;
        }

        protected GameSession GivenGameExists(User owner, DateTime dateCreated, User otherPlayer = null)
        {
            var session = new GameSession(owner) {DateCreated = dateCreated};

            if (otherPlayer != null)
            {
                session.AddSecondUser(otherPlayer);
            }

            Repository.Insert(session);
           
            return session;
        }

        protected GameSession GivenClosedGameExists(User owner, User otherPlayer = null)
        {
            var session = new GameSession(owner);

            if (otherPlayer != null)
            {
                session.AddSecondUser(otherPlayer);
            }

            session.EndSession();

            Repository.Insert(session);

            return session;
        }
    }
}