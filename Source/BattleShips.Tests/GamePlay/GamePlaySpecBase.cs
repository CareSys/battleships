﻿using System.Collections.Generic;
using BattleShips.Services;
using Moq;

namespace BattleShips.Tests.GamePlay
{
    public class GamePlaySpecBase : SpecBase
    {
        protected Mock<IPublishService> PublishService = new Mock<IPublishService>();
        protected readonly Mock<IRepository> Repository = new Mock<IRepository>();
        protected GamePlayService GamePlayService;
        protected Game Game;
        
        protected void InitialiseService()
        {
            GamePlayService = new GamePlayService(Repository.Object, PublishService.Object);
        }
        
        protected void GivenGameExists()
        {
            Game = new Game();

            Game.PlayerId1 = 1;
            Game.PlayerId2 = 2;

            Repository.Object.Insert(Game);

            Repository.Setup(r => r.Get<Game>(It.IsAny<int>())).Returns(Game);
        }

        protected void GivenItIsPlayer1Turn()
        {
            Game.CurrentPlayer = PlayerDesignation.Player1;
        }

        protected void GivenItIsPlayer2Turn()
        {
            Game.CurrentPlayer = PlayerDesignation.Player2;
        }

        protected void GivenPlayer1Attacks(Coordinate coordinate)
        {
            GamePlayService.Attack(Game.PlayerId1, Game.Id, coordinate);
        }

        protected void GivenPlayer2Attacks(Coordinate coordinate)
        {
            GamePlayService.Attack(Game.PlayerId2, Game.Id, coordinate);
        }

        protected void GivenPlayer1HasShips(params Coordinate[] coordinates)
        {
            Game.Player1Ships = new List<Coordinate>(coordinates);
        }

        protected void GivenPlayer2HasShips(params Coordinate[] coordinates)
        {
            Game.Player2Ships = new List<Coordinate>(coordinates);
        }
    }
}