﻿using BattleShips.Events;
using BattleShips.Services;
using Moq;
using NUnit.Framework;

namespace BattleShips.Tests.GamePlay
{
    public class ShootingAndHitting : GamePlaySpecBase
    {
        private AttackResultEvent _lastAttackResult;
        
        protected override void Given()
        {
            InitialiseService();

            PublishService.Setup(s => s.PublishToUser(It.IsAny<int>(), It.IsAny<AttackResultEvent>()))
                .Callback<int, IEvent>((i, r) => _lastAttackResult = (AttackResultEvent)r);

            GivenGameExists();

            GivenPlayer1HasShips(
                new Coordinate(1, 1),
                new Coordinate(2, 1),
                new Coordinate(3, 1),
                new Coordinate(4, 1),
                new Coordinate(5, 1));

            GivenItIsPlayer2Turn();
        }

        protected override void When()
        {
            GamePlayService.Attack(Game.PlayerId2, Game.Id, new Coordinate(2, 1));
        }

        [Then]
        public void Hit()
        {
            Assert.AreEqual(AttackResult.Hit, _lastAttackResult.Result);
        }

        [Then]
        public void GameStateToBothPlayers()
        {
            PublishService.Verify(p => p.PublishToUser(Game.PlayerId1, It.IsAny<AttackResultEvent>()), Times.Once);
            PublishService.Verify(p => p.PublishToUser(Game.PlayerId2, It.IsAny<AttackResultEvent>()), Times.Once);
        }

        [Then]
        public void Player2Turn()
        {
            Assert.AreEqual(Game.CurrentPlayer, PlayerDesignation.Player2);
        }
    }
}