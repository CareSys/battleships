﻿using BattleShips.Events;
using BattleShips.Services;
using Moq;
using NUnit.Framework;

namespace BattleShips.Tests.GamePlay
{
    public class ShootingAndSinkingLastShip : GamePlaySpecBase
    {
        //Given all ships have been hit except one
        //And its player 1's turn
        //And its best of 1
        //When they hit the last square
        //Then the game ends
        //And the win count increments
        //And the session closes

        private AttackResultEvent _lastAttackResult;
        private Coordinate _player1LastCoordinate;

        protected override void Given()
        {
            InitialiseService();
            
            PublishService.Setup(s => s.PublishToUser(It.IsAny<int>(), It.IsAny<AttackResultEvent>()))
                .Callback<int, IEvent>((i, r) => _lastAttackResult = (AttackResultEvent)r);

            GivenGameExists();
            GivenPlayer1HasShips(new Coordinate(1, 1), new Coordinate(2, 1));
            GivenPlayer2HasShips(new Coordinate(9, 10), new Coordinate(10, 10));
            GivenItIsPlayer2Turn();
            GivenPlayer2Attacks(new Coordinate(1, 1));

            _player1LastCoordinate = new Coordinate(2, 1);

            PublishService.ResetCalls();
        }

        protected override void When()
        {
            GamePlayService.Attack(Game.PlayerId2, Game.Id, _player1LastCoordinate);
        }

        [Then]
        public void HitAndWin()
        {
            Assert.AreEqual(AttackResult.HitAndWin, _lastAttackResult.Result);
        }

        [Then]
        public void GameStateToBothPlayers()
        {
            PublishService.Verify(p => p.PublishToUser(Game.PlayerId1, It.IsAny<AttackResultEvent>()), Times.Once);
            PublishService.Verify(p => p.PublishToUser(Game.PlayerId2, It.IsAny<AttackResultEvent>()), Times.Once);
        }

        [Then]
        public void GameIsFinished()
        {
            Assert.AreEqual(Game.State, GameState.Finished);
        }
    }
}