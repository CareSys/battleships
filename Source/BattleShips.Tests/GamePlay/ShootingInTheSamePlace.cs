﻿using System;
using BattleShips.Services;
using Moq;
using NUnit.Framework;

namespace BattleShips.Tests.GamePlay
{
    public class ShootingInTheSamePlace : GamePlaySpecBase
    {
        private Exception _exception;
        private Coordinate _player2PreviousShot;

        protected override void Given()
        {
            InitialiseService();

            GivenGameExists();
            GivenPlayer1HasShips(new Coordinate(10, 10));
            GivenPlayer2HasShips(new Coordinate(8, 8));
            GivenItIsPlayer1Turn();
            GivenPlayer1Attacks(new Coordinate(1, 1));
            GivenPlayer2Attacks(new Coordinate(1, 1));
            
            _player2PreviousShot = new Coordinate(1, 1);

            PublishService.ResetCalls();
        }

        protected override void When()
        {
            try
            {
                GamePlayService.Attack(Game.PlayerId1, Game.Id, _player2PreviousShot);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then]
        public void ExceptionIsThrown()
        {
            Assert.IsNotNull(_exception);
        }

        [Then]
        public void NoGameStateIsPublished()
        {
            PublishService.Verify(p => p.PublishToUser(It.IsAny<int>(), It.IsAny<IEvent>()), Times.Never);
        }
    }
}