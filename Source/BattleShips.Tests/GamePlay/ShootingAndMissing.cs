﻿using BattleShips.Events;
using BattleShips.Services;
using Moq;
using NUnit.Framework;

namespace BattleShips.Tests.GamePlay
{
    public class ShootingAndMissing : GamePlaySpecBase
    {
        private AttackResultEvent _lastAttackResult;

        protected override void Given()
        {
            InitialiseService();

            PublishService.Setup(s => s.PublishToUser(It.IsAny<int>(), It.IsAny<AttackResultEvent>()))
                .Callback<int, IEvent>((i, r) => _lastAttackResult = (AttackResultEvent)r);

            GivenGameExists();
            GivenPlayer2HasShips(new Coordinate(1, 1));
            GivenItIsPlayer1Turn();
        }

        protected override void When()
        {
            GamePlayService.Attack(Game.PlayerId1, Game.Id, new Coordinate(10, 10));
        }

        [Then]
        public void Miss()
        {
            Assert.AreEqual(AttackResult.Miss, _lastAttackResult.Result);
        }

        [Then]
        public void GameStateToBothPlayers()
        {
            PublishService.Verify(p => p.PublishToUser(Game.PlayerId1, It.IsAny<AttackResultEvent>()), Times.Once);
            PublishService.Verify(p => p.PublishToUser(Game.PlayerId2, It.IsAny<AttackResultEvent>()), Times.Once);
        }

        [Then]
        public void Player2Turn()
        {
            Assert.AreEqual(Game.CurrentPlayer, PlayerDesignation.Player2);
        }
    }
}