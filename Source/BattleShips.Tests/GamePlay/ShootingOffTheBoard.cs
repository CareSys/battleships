﻿using System;
using BattleShips.Events;
using Moq;
using NUnit.Framework;

namespace BattleShips.Tests.GamePlay
{
    public class ShootingOffTheBoard : GamePlaySpecBase
    {
        private Exception _exception;

        protected override void Given()
        {
             InitialiseService();

            GivenGameExists();
            GivenItIsPlayer2Turn();
        }

        protected override void When()
        {
            try
            {
                GamePlayService.Attack(Game.PlayerId2, Game.Id, new Coordinate(2, 11));
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then]
        public void ExceptionIsThrown()
        {
            Assert.IsNotNull(_exception);
        }

        [Then]
        public void NoGameStateIsPublished()
        {
            PublishService.Verify(p => p.PublishToUser(It.IsAny<int>(), It.IsAny<AttackResultEvent>()), Times.Never);
        }
    }
}