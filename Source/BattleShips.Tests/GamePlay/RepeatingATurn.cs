﻿using System;
using BattleShips.Services;
using Moq;
using NUnit.Framework;

namespace BattleShips.Tests.GamePlay
{
    public class RepeatingATurn : GamePlaySpecBase
    {
        private Exception _exception;

        protected override void Given()
        {
            InitialiseService();

            GivenGameExists();
            GivenItIsPlayer2Turn();
            GivenPlayer2Attacks(new Coordinate(1, 1));

            PublishService = new Mock<IPublishService>();
        }

        protected override void When()
        {
            try
            {
                GamePlayService.Attack(Game.PlayerId2, Game.Id, new Coordinate(1, 1));
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then]
        public void ExceptionIsThrown()
        {
            Assert.IsNotNull(_exception);
        }

        [Then]
        public void NoGameStateIsPublished()
        {
            PublishService.Verify(p => p.PublishToUser(It.IsAny<int>(), It.IsAny<IEvent>()), Times.Never);
        }
    }
}